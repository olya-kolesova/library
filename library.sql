SELECT
	authors.id, authors.name,
    COUNT(books.id) AS c
FROM authors
JOIN books ON authors.id = books.author_id
GROUP BY authors.id
ORDER BY c DESC
